(deftype uint8 () '(unsigned-byte 8))
(deftype uint12 () '(unsigned-byte 12))
(deftype uint16 () '(unsigned-byte 16))

;; CHIP-8 machines have a main memory of 4K bytes
(defconstant +memory-size+ (* 4 1024))

;; CHIP-8 machines have a maximum stack depth of 16
(defconstant +stack-limit+ 16)

;; CHIP-8 machines have 16 main registers
(defconstant +register-count+ 16)

;; CHIP-8 machines maintain a segment of memory for storing sprites of the
;; characters used for representing hexidecimal values.
(defconstant +hex-sprites+
  (list
   ;; "0"
   #b11110000
   #b10010000
   #b10010000
   #b10010000
   #b11110000

   ;; "1"
   #b00100000
   #b01100000
   #b00100000
   #b00100000
   #b01110000

   ;; "2"
   #b11110000
   #b00010000
   #b11110000
   #b10000000
   #b11110000

   ;; "3"
   #b11110000
   #b00010000
   #b11110000
   #b00010000
   #b11110000

   ;; "4"
   #b10010000
   #b10010000
   #b11110000
   #b00010000
   #b00010000

   ;; "5"
   #b11110000
   #b10000000
   #b11110000
   #b00010000
   #b11110000

   ;; "6"
   #b11110000
   #b10000000
   #b11110000
   #b10010000
   #b11110000

   ;; "7"
   #b11110000
   #b00010000
   #b00100000
   #b01000000
   #b01000000

   ;; "8"
   #b11110000
   #b10010000
   #b11110000
   #b10010000
   #b11110000

   ;; "9"
   #b11110000
   #b10010000
   #b11110000
   #b00010000
   #b11110000

   ;; "A"
   #b11110000
   #b10010000
   #b11110000
   #b10010000
   #b10010000

   ;; "B"
   #b11100000
   #b10010000
   #b11100000
   #b10010000
   #b11100000

   ;; "C"
   #b11110000
   #b10000000
   #b10000000
   #b10000000
   #b11110000

   ;; "D"
   #b11100000
   #b10010000
   #b10010000
   #b10010000
   #b11100000

   ;; "E"
   #b11110000
   #b10000000
   #b11110000
   #b10000000
   #b11110000

   ;; "F"
   #b11110000
   #b10000000
   #b11110000
   #b10000000
   #b10000000))
