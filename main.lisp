(defstruct cpu
  ;; Pseudo-registers
  (prog-counter 0 :type integer)
  (stack-pointer 0 :type unsigned-byte)
  (sound-timer 0 :type unsigned-byte)
  (delay-timer 0 :type unsigned-byte)
  ;; CPU Registers
  (v-regs (make-array '(16)
                      :element-type 'unsigned-byte
                      :initial-element 0)
   :type array)
  (i-reg 0 :type integer)
  ;; Main memory
  (memory (make-array '(4096)
                      :element-type 'unsigned-byte
                      :initial-element 0)
   :type array))

(defmacro with-cpu (cpu &body body)
  "Establishes a lexical environment for referring to the slots of a given CPU
as if they were variables."
  `(with-slots (prog-counter
                stack-pointer
                sound-timer
                delay-timer
                v-regs
                i-reg
                memory)
       ,cpu
     ,@body))

(defstruct chip
  (cpu (make-cpu) :type cpu)
  (display (make-array '(32 64)
                       :element-type 'bit
                       :initial-element 0)
   :type array))

