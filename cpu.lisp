(load "cpu-constants.lisp")

(defstruct cpu
  ;; Pseudo-registers
  (prog-counter 0 :type uint16)
  (stack-pointer 0 :type uint8)
  (sound-timer 0 :type uint8)
  (delay-timer 0 :type uint8)

  ;; Main registers (denoted Vx where x is a hex value of the given register)
  (v-regs (make-array +register-count+
                      :element-type 'uint8
                      :initial-element 0)
   :type array
   :read-only t)

  ;; Address register (denoted I)
  ;; The I register is technically 16 bits but only the lower 12 are used
  (i-reg 0 :type uint12)

  ;; Main memory
  (memory (make-array +memory-size+
                      :element-type 'uint8
                      :initial-element 0)
   :type array
   :read-only t)

  ;; Stack
  (stack (make-array +stack-limit+
                     :element-type 'uint16
                     :initial-element 0
                     :fill-pointer 0)
   :type vector))

(defmacro with-cpu (cpu &body body)
  "Establishes a lexical environment for referring to the slots of a given CPU
   as if they were variables."
  `(with-slots (prog-counter
                stack-pointer
                sound-timer
                delay-timer
                v-regs
                i-reg
                memory)
       ,cpu
     ,@body))

(defun load-into-mem (memory start data)
  "Loads a sequence of bytes into memory, starting at the specified location."
  (loop for datum in data
        for idx = start then (+ 1 idx)
        do (setf (aref memory idx) datum)))

(defun read-from-mem (memory start num-bytes)
  "Reads a sequence of bytes from memory, starting at the specified location,
   and returns them."
  (loop for idx = start then (+ 1 idx)
        while (< idx num-bytes)
        collect (aref memory idx)))

(defun init-cpu (cpu)
  "Initializes the given CHIP-8 CPU."
  (with-cpu cpu
    (setf prog-counter #x200)
    (load-into-mem memory 0 +hex-sprites+)))

(defun read-instr (cpu)
  "Reads an instruction (two bytes) from memory at the location currently
   pointed to by the program counter."
  (with-cpu cpu
    (let ((high (aref memory prog-counter))
          (low (aref memory (+ 8 prog-counter))))
      (logior (ash high 8) low))))
