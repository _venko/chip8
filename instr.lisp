(defun parse-instruction (word)
  "Parses an instruction from a 16-bit word"
  (cond ((eql #x00E0 word)
         (list 'CLS nil))

        ((eql #x00EE word)
         (list 'RET nil))

        ((eql #x1000 (logand #xF000 word))
         (list 'JMP (logand #x0FFF word)))

        ((eql #x2000 (logand #xF000 word))
         (list 'CALL (logand #x0FFF word)))

        ((eql #x3000 (logand #xF000 word))
         (let ((x (ash (logand #x0F00 word) -8))
               (kk (logand #x0FF word)))
           (list 'SEI x kk)))

        ((eql #x4000 (logand #xF000 word))
         (let ((x (ash (logand #x0F00 word) -8))
               (kk (logand #x0FF word)))
           (list 'SNE x kk)))

        ((eql #x5000 (logand #xF00F word))
         (let ((x (ash (logand #x0F00 word) -8))
               (y (ash (logand #x00F0 word) -4)))
           (list 'SE x y)))

        ((eql #x6000 (logand #xF000 word))
         (let ((x (ash (logand #x0F00 word) -8))
               (kk (logand #x0FF word)))
           (list 'LDI x kk)))

        ((eql #x7000 (logand #xF000 word))
         (let ((x (ash (logand #x0F00 word) -8))
               (kk (logand #x0FF word)))
           (list 'ADDI x kk)))

        ((eql #x8000 (logand #xF00F word))
         (let ((x (ash (logand #x0F00 word) -8))
               (y (ash (logand #x00F0 word) -4)))
           (list 'LD x y)))

        ((eql #x8001 (logand #xF00F word))
         (let ((x (ash (logand #x0F00 word) -8))
               (y (ash (logand #x00F0 word) -4)))
           (list 'OR x y)))

        ((eql #x8002 (logand #xF00F word))
         (let ((x (ash (logand #x0F00 word) -8))
               (y (ash (logand #x00F0 word) -4)))
           (list 'AND x y)))

        ((eql #x8003 (logand #xF00F word))
         (let ((x (ash (logand #x0F00 word) -8))
               (y (ash (logand #x00F0 word) -4)))
           (list 'XOR x y)))

        ((eql #x8004 (logand #xF00F word))
         (let ((x (ash (logand #x0F00 word) -8))
               (y (ash (logand #x00F0 word) -4)))
           (list 'ADD x y)))

        ((eql #x8005 (logand #xF00F word))
         (let ((x (ash (logand #x0F00 word) -8))
               (y (ash (logand #x00F0 word) -4)))
           (list 'SUB x y)))

        ((eql #x8006 (logand #xF00F word))
         (let ((x (ash (logand #x0F00 word) -8))
               (y (ash (logand #x00F0 word) -4)))
           (list 'SHR x y)))

        ((eql #x8007 (logand #xF00F word))
         (let ((x (ash (logand #x0F00 word) -8))
               (y (ash (logand #x00F0 word) -4)))
           (list 'SUBN x y)))

        ((eql #x800E (logand #xF00F word))
         (let ((x (ash (logand #x0F00 word) -8))
               (y (ash (logand #x00F0 word) -4)))
           (list 'SHL x y)))

        ((eql #x9000 (logand #xF00F word))
         (let ((x (ash (logand #x0F00 word) -8))
               (y (ash (logand #x00F0 word) -4)))
           (list 'SNE x y)))

        ((eql #xA000 (logand #xF000 word))
         (list 'LD (logand #x0FFF word)))

        ((eql #xB000 (logand #xF000 word))
         (list 'JP (logand #x0FFF word)))

        ((eql #xC000 (logand #xF000 word))
         (let ((x (ash (logand #x0F00 word) -8))
               (kk (logand #x0FF word)))
           (list 'RND x kk)))

        ((eql #xD000 (logand #xF000 word))
         (let ((x (ash (logand #x0F00 word) -8))
               (y (ash (logand #x00F0 word) -4))
               (n (logand #x000F word)))
           (list 'DRW x y n)))

        ((eql #xE09E (logand #xF0FF word))
         (let ((x (ash (logand #x0F00 word) -8)))
           (list 'SKP x)))

        ((eql #xE0A1 (logand #xF0FF word))
         (let ((x (ash (logand #x0F00 word) -8)))
           (list 'SKNP x)))

         ((eql #xF007 (logand #xF0FF word))
          (let ((x (ash (logand #x0F00 word) -8)))
            (list 'LOAD-DELAY x)))

         ((eql #xF00A (logand #xF0FF word))
          (let ((x (ash (logand #x0F00 word) -8)))
            (list 'LOAD-KEY x)))

         ((eql #xF015 (logand #xF0FF word))
          (let ((x (ash (logand #x0F00 word) -8)))
            (list 'SET-DELAY x)))

         ((eql #xF018 (logand #xF0FF word))
          (let ((x (ash (logand #x0F00 word) -8)))
            (list 'SET-SOUND x)))

         ((eql #xF018 (logand #xF0FF word))
          (let ((x (ash (logand #x0F00 word) -8)))
            (list 'SET-SOUND x)))

         ((eql #xF01E (logand #xF0FF word))
          (let ((x (ash (logand #x0F00 word) -8)))
            (list 'ADD-I x)))

         ((eql #xF029 (logand #xF0FF word))
          (let ((x (ash (logand #x0F00 word) -8)))
            (list 'SET-I-SPRITE x)))

         ((eql #xF033 (logand #xF0FF word))
          (let ((x (ash (logand #x0F00 word) -8)))
            (list 'SAVE-REGISTER-DIGITS x)))

         ((eql #xF055 (logand #xF0FF word))
          (let ((x (ash (logand #x0F00 word) -8)))
            (list 'SAVE-REGISTERS x)))

         ((eql #xF065 (logand #xF0FF word))
          (let ((x (ash (logand #x0F00 word) -8)))
            (list 'LOAD-REGISTERS x)))))
